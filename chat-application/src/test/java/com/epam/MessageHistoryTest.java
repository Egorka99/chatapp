package com.epam;

import com.epam.chat.server.MsgStory;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.Reader;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class MessageHistoryTest {

    @Test
    public void addingHistoryTest() {
        MsgStory msgStory = MsgStory.getInstance();
        msgStory.addMessageInStory("1");
        msgStory.addMessageInStory("2");
        msgStory.addMessageInStory("3");
        msgStory.addMessageInStory("4");
        msgStory.addMessageInStory("5");

        List<String> expected = Arrays.asList("1","2","3","4","5");

        assertEquals(expected,msgStory.getCurrentHistory());

    }

    @Test
    public void addingHistoryMoreThanFiveElemTest() {
        MsgStory msgStory = MsgStory.getInstance();
        msgStory.addMessageInStory("1");
        msgStory.addMessageInStory("2");
        msgStory.addMessageInStory("3");
        msgStory.addMessageInStory("4");
        msgStory.addMessageInStory("5");
        msgStory.addMessageInStory("6");
        msgStory.addMessageInStory("7");

        List<String> expected = Arrays.asList("3","4","5","6","7");

        assertEquals(expected,msgStory.getCurrentHistory());
    }

    @Test
    public void readAndSaveHistoryFromFileTest() {
        MsgStory msgStory = MsgStory.getInstance();

        msgStory.addMessageInStory("1");
        msgStory.addMessageInStory("2");
        msgStory.addMessageInStory("3");
        msgStory.addMessageInStory("4");
        msgStory.addMessageInStory("5");

        msgStory.saveHistoryInFile();
        msgStory.readHistoryFromFile();

        List<String> expected = Arrays.asList("1","2","3","4","5");

        assertEquals(expected,msgStory.getCurrentHistory());
    }
    @Test
    public void readAndSaveHistoryMoreThenFiveFromFileTest() {
        MsgStory msgStory = MsgStory.getInstance();

        msgStory.addMessageInStory("1");
        msgStory.addMessageInStory("2");
        msgStory.addMessageInStory("3");
        msgStory.addMessageInStory("4");
        msgStory.addMessageInStory("5");
        msgStory.addMessageInStory("6");
        msgStory.addMessageInStory("7");

        msgStory.saveHistoryInFile();
        msgStory.readHistoryFromFile();

        List<String> expected = Arrays.asList("3","4","5","6","7");

        assertEquals(expected,msgStory.getCurrentHistory());
    }


}
