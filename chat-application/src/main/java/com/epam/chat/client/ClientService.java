package com.epam.chat.client;

import com.epam.chat.Config;

import java.io.*;
import java.net.Socket;


/**
 * The instance of this class works with one client, sending his messages to the server
 * and receiving messages from the server
 */
public class ClientService {

    /**
     * socket receiving messages from the server
     */
    private Socket socket;

    /**
     * Input Stream from socket
     */
    private BufferedReader in;

    /**
     * Output Stream from socket
     */
    private BufferedWriter out;

    /**
     * Read stream from client console
     */
    private BufferedReader inputUser;

    /**
     * The word by which the client leaves the chat (define in properties)
     */
    private String stopWord;

    /**
     * Current client
     */
    private Client client;

    /**
     * Message max length (define in properties)
     */
    private int messageMaxLength;

    /**
     * Init new ClientService
     * @param addr server address
     * @param port server port
     * @param client client entity
     * @throws IOException config exceptions
     */
    public ClientService(String addr, int port, Client client) throws IOException {
        this.client = client;
        this.socket = new Socket(addr, port);
        Config config = Config.getInstance();
        stopWord = config.getStopWord();
        messageMaxLength = config.getMessageMaxLength();

        try {
            inputUser = new BufferedReader(new InputStreamReader(System.in));
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            new ReadMsg().start();
            new WriteMsg().start();
        }
        catch (IOException ex) {
            ClientService.this.downService();
        }
    }

    /**
     * Shutdown client, close all streams
     */
    private void downService() {
        try {
            if (!socket.isClosed()) {
                socket.close();
                in.close();
                out.close();
            }
        } catch (IOException ignored) {}
    }

    /**
     * Thread, reading messages from the server
     */
    private class ReadMsg extends Thread {
        @Override
        public void run() {
            String str;
            try {
                while (true) {
                    str = in.readLine();
                    if (str.equals(stopWord)) {
                        ClientService.this.downService();
                        break;
                    }
                    System.out.println(str);
                }
            } catch (IOException e) {
                ClientService.this.downService();
            }
        }
    }

    /**
     * Thread writing messages to the server
     */
    public class WriteMsg extends Thread {

        @Override
        public void run() {
            while (true) {
                String userWord = "";
                try {
                    while (userWord.length() == 0) {
                        userWord = inputUser.readLine();
                    }
                    if (userWord.length() > messageMaxLength) {
                        System.err.println("Ошибка. Максимальная длина сообщения " + messageMaxLength + " символов");
                        continue;
                    }
                    if (userWord.equals(stopWord)) {
                        out.write(stopWord + "\n");
                        ClientService.this.downService();
                        break;
                    } else {
                        out.write(client.getName() + ": " + userWord + "\n");
                    }
                    out.flush();
                } catch (IOException e) {
                    ClientService.this.downService();
                }


            }
        }
    }


}
