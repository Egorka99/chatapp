package com.epam.chat.client;

import com.epam.chat.Config;

import java.io.IOException;

/**
 * Client entity
 */
public class Client {
    /**
     * Client name
     */
    private String name;

    /**
     * maximum client name length (taken from config)
     */
    private int nameMaxLength;

    /**
     * Initialization new client
     * @param name client's name
     * @throws ClientException maximum name length exceeded
     * @throws IOException config exception
     */
    public Client(String name) throws ClientException, IOException {

        Config config = Config.getInstance();
        nameMaxLength = config.getClientNameMaxLength();
        if (name.length() > nameMaxLength) throw new ClientException("Максимальная длина имени: " + nameMaxLength + " символов");
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
