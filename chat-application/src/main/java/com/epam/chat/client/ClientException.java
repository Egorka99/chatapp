package com.epam.chat.client;

/**
 * Exceptions with client
 */
public class ClientException extends Exception {
    public ClientException(String message) {
        super(message);
    }
}
