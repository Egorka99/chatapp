package com.epam.chat.server;

import com.epam.chat.Config;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MainServer {

    private static List<ClientHandler> clientHandlerList = new ArrayList<>();
    private static MsgStory history;

    public static MsgStory getMessageHistory() {
        return history;
    }

    public static List<ClientHandler> getClientHandlerList() {
        return clientHandlerList;
    }

    public static void main(String[] args) throws IOException {

        Config config = Config.getInstance();

        int port = config.getPort();

        try (ServerSocket server = new ServerSocket(port)) {
            history = MsgStory.getInstance();
            history.readHistoryFromFile();
            System.out.println("Сервер запущен...");
            historySaveThreadStart();
            while (true) {
                Socket socket = server.accept();
                try {
                    clientHandlerList.add(new ClientHandler(socket));
                    System.out.println("Новый пользователь вошел в чат");
                } catch (IOException | InterruptedException e) {
                    socket.close();
                }
            }
        }
    }

    private static void historySaveThreadStart() {
        new Thread(() -> {
            System.out.println("Для сохранения истории сообщений напишите save");
            String input = new Scanner(System.in).next();
            if (input.equals("save")) {
                history.saveHistoryInFile();
                System.out.println("История сохранена!");
            }
        }).start();
    }




}
