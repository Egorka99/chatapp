package com.epam.chat.server;

import com.epam.chat.Config;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Message history storage. Old history in file, current history in list.
 */
public class MsgStory {

    /**
     * History of the current session
     */
    private List<String> currentHistory;

    /**
     * Count of message in current history
     */
    private int msgCount;

    /**
     * Singleton pattern
     */
    private static MsgStory instance;

    /**
     * Application config object
     */
    private static Config config;

    /**
     * Singleton pattern
     */
    private MsgStory()
        {
            currentHistory = new ArrayList<>();
            msgCount = 5;
            try {
                config = Config.getInstance();
            }
            catch (IOException ex) {
                System.err.println("Ошибка: " + ex.getMessage());
            }
        }

    /**
     * Get an object of this class
     */
    public static synchronized MsgStory getInstance() {
        if (instance == null) {
            instance = new MsgStory();
        }
        return instance;
        }

    /**
     * Adding new message in current session
     */
    public void addMessageInStory(String el) {
        if (currentHistory.size() >= msgCount) {
            currentHistory.remove(0);
            currentHistory.add(el);
        } else {
            currentHistory.add(el);
        }
    }

    /**
     * Saving history in file (file path define in .properties)
     */
    public void saveHistoryInFile()  {
            try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(config.getHistoryFilePath(), false)))
            {
                for (String s : currentHistory) {
                    oos.writeObject(s);
                }
            }
            catch (FileNotFoundException ex) {
                System.err.println("Файл с историей не найден");
            }
            catch(IOException ex){
                System.err.println("Ошибка: " + ex.getMessage());
            }
        }

    /**
     * Read history from file (history record in current)
     */
    public void readHistoryFromFile() {
        currentHistory.clear();
        try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream(config.getHistoryFilePath())))
        {
            readAllHistory(ois);
        }
        catch (EOFException ex) {
            System.err.println("Файл с историей пуст");
        }
        catch (FileNotFoundException ex) {
            System.err.println("Файл с историей не найден");
        }
        catch(IOException | ClassNotFoundException ex){
            System.err.println("Ошибка: " + ex.getMessage());
        }
    }

    /**
     * Print history in specified stream
     * @param writer input stream
     */
    public void printStory(BufferedWriter writer) {
            if(currentHistory.size() > 0) {
                try {
                    for (String vr : currentHistory) {
                        writer.write(vr + "\n");
                    }
                    writer.flush();
                } catch (IOException ignored) {}

            }
        }

    /**
     * Read all history from file
     * @param ois file stream
     * @throws IOException read object exceptions
     * @throws ClassNotFoundException string not found
     */
    private void readAllHistory(ObjectInputStream ois) throws IOException, ClassNotFoundException {
        try {
            while(true) {
                currentHistory.add((String) ois.readObject());
            }
        } catch (EOFException ignored) {}
    }

    public List<String> getCurrentHistory() {
        return currentHistory;
    }
}
