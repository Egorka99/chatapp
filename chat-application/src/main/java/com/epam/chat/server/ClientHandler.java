package com.epam.chat.server;

import com.epam.chat.Config;

import java.io.*;
import java.net.Socket;

/**
 * Server side client processing
 */
public class ClientHandler extends Thread{

    /**
     * Socket, receiving messages from the client
     */
    private Socket socket;

    /**
     * Input Stream from socket
     */
    private BufferedReader in;

    /**
     * Output Stream from socket
     */
    private BufferedWriter out;

    /**
     * The word by which the client leaves the chat (define in properties)
     */
    private String stopWord;

    /**
     * Init new ClientHandler
     * @param socket Socket, receiving messages from the client
     * @throws IOException config exceptions and stream exceptions
     */
    public ClientHandler(Socket socket) throws IOException, InterruptedException {
        this.socket = socket;
        in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));

        start();
        Thread.sleep(100);
        MainServer.getMessageHistory().printStory(out);

        Config config = Config.getInstance();
        stopWord = config.getStopWord();
    }

    /**
     * reading messages from the client
     */
    @Override
    public void run() {
        String word;
        try {
            try {
                while (true) {
                    word = in.readLine();
                    if(word.equals(stopWord)) {
                        this.downService();
                        break;
                    }
                    System.out.println(word);
                    MainServer.getMessageHistory().addMessageInStory(word);
                    for (ClientHandler vr : MainServer.getClientHandlerList()) {
                        vr.send(word);
                    }
                }
            } catch (NullPointerException ignored) {}
        } catch (IOException ignored) {
            this.downService();
        }
    }

    /**
     * sending one message to the client on the specified stream
     * @param msg client msg
     */
    private void send(String msg) {
        try {
            out.write(msg + "\n");
            out.flush();
        } catch (IOException ignored) {}

    }

    /**
     * server shutdown
     * interruption of itself as a thread and removal from the list of threads
     */
    private void downService() {
        try {
            if(!socket.isClosed()) {
                socket.close();
                in.close();
                out.close();
                for (ClientHandler vr : MainServer.getClientHandlerList()) {
                    if(vr.equals(this)) vr.interrupt();
                    MainServer.getClientHandlerList().remove(this);
                }
            }
        } catch (IOException e) {
            System.err.println("Ошибка : " + e.getMessage());
        }
        catch (RuntimeException ignored) {}

    }
}
