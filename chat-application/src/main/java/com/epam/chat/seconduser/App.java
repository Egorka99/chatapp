package com.epam.chat.seconduser;


import com.epam.chat.Config;
import com.epam.chat.client.ClientException;
import com.epam.chat.client.ClientService;
import com.epam.chat.client.Client;

import java.io.IOException;
import java.util.Scanner;

/**
 * Client entry point
 */
public class App {

    /**
     * Server ip address
     */
    public static String ipAddr;

    /**
     * Server port
     */
    public static int port;

    /**
     * Client entity
     */
    public static Client client;

    /**
     * Application config
     */
    private static Config config;


    /**
     * Creating a client connection with the recognized address and port number
     */
    public static void main(String[] args)  {

        try {
            config = Config.getInstance();
        }
        catch (IOException ex) {
            System.err.println("Ошибка: " + ex.getMessage());
        }

        ipAddr = config.getIpAddr();
        port = config.getPort();

        Scanner scanner = new Scanner(System.in);

        System.out.println("Чат");
        System.out.println("Введите ваше имя: ");

        String loginName = scanner.next();

        try {
            client = new Client(loginName);
        }
        catch (ClientException | IOException ex) {
            System.err.println(ex.getMessage());
            return;
        }

        try {
            new ClientService(ipAddr,port,client);
            System.out.println("Подключено!");
            System.out.println("Чтобы выйти из чата, напишите " + config.getStopWord());
        }
        catch (IOException ex) {
            System.err.println("Не удалось подключится к серверу");
        }


    }
}
