package com.epam.chat;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Config {

    private static Config instance;

    private String configFilePath = "application.properties";

    private String stopWord;
    private String ipAddr;
    private int port;
    private String historyFilePath;
    private int clientNameMaxLength;
    private int messageMaxLength;

    private Config() throws IOException {

        InputStream is = getClass().getClassLoader().getResourceAsStream(configFilePath);
        Properties property = new Properties();
        property.load(is);

        stopWord = property.getProperty("clientExitWord");
        ipAddr = property.getProperty("ipaddress");
        port = Integer.parseInt(property.getProperty("port"));
        historyFilePath = property.getProperty("messageHistoryFilePath");
        clientNameMaxLength = Integer.parseInt(property.getProperty("clientNameMaxLength"));
        messageMaxLength = Integer.parseInt(property.getProperty("messageMaxLength"));
    }

    public static synchronized Config getInstance() throws IOException {
        if (instance == null) {
            instance = new Config();
        }
        return instance;
    }


    public int getMessageMaxLength() {
        return messageMaxLength;
    }

    public int getClientNameMaxLength() {
        return clientNameMaxLength;
    }

    public String getStopWord() {
        return stopWord;
    }

    public String getIpAddr() {
        return ipAddr;
    }

    public int getPort() {
        return port;
    }

    public String getHistoryFilePath() {
        return historyFilePath;
    }
}

